package me.robertnisipeanu.androidnews.LoginScreen;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;

import java.util.HashMap;
import java.util.Map;

import me.robertnisipeanu.androidnews.HomeScreen.HomeFragment;
import me.robertnisipeanu.androidnews.MainActivity;
import me.robertnisipeanu.androidnews.R;
import me.robertnisipeanu.androidnews.Tags;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {

    private static final String TAG = "LoginFragment";

    private FirebaseAuth mAuth;
    private FirebaseFirestore database;
    private GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN = 9001;

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_login, container, false);

        ImageButton buttonLeft = view.findViewById(R.id.navButton_left);
        TextView navText = view.findViewById(R.id.navText);

        buttonLeft.setImageResource(R.drawable.ic_back);
        buttonLeft.setVisibility(View.VISIBLE);
        buttonLeft.setTag(Tags.BACK_BUTTON);
        buttonLeft.setOnClickListener(((MainActivity)getActivity())::onButtonBackClick);

        navText.setText(getString(R.string.login));

        runAtVisible(view);

        view.findViewById(R.id.button_Signin).setOnClickListener(this::onSigninClick);
        return view;
    }

    public void runAtVisible(View view){
        // Initialize firebase variables
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(getActivity().getBaseContext(), gso);
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseFirestore.getInstance();
        //Finished firebase variables initialization
        if(mAuth.getCurrentUser() != null) view.findViewById(R.id.button_Signin).setEnabled(false);
        else {
            view.findViewById(R.id.button_Signin).setEnabled(true);
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(!hidden) runAtVisible(getView());
    }

    public void onSigninClick(View view) {
        view.setEnabled(false);
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == RC_SIGN_IN) {
            try {
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseSignInGoogle(account);
            } catch(ApiException e){
                Log.d(TAG, "Auth cancelled");
                getView().findViewById(R.id.button_Signin).setEnabled(true);
            }
        }
    }

    private void firebaseSignInGoogle(GoogleSignInAccount account) {
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()) {
                        Log.d(TAG, "Auth complete");
                        Map<String, Object> user = new HashMap<>();
                        user.put("userid", mAuth.getCurrentUser().getUid());
                        user.put("name", mAuth.getCurrentUser().getDisplayName());
                        database.collection("users").document(mAuth.getCurrentUser().getUid()).set(user, SetOptions.merge()).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()) {
                                    Log.d(TAG, "Updated user in database");
                                    ((MainActivity)getActivity()).goToPage("LOGIN");
                                    ((HomeFragment)getActivity().getSupportFragmentManager().findFragmentByTag("HOME")).moderatorCheck();
                                }
                                else {
                                    Log.d(TAG, "Failed updating user in database, aborting login...");
                                    // TODO: Logout and make user log in again
                                }
                            }
                        });
                    }
                    else Log.d(TAG, "Auth failed");
                });
    }
}
