package me.robertnisipeanu.androidnews.LoginScreen;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.auth.FirebaseAuth;

import me.robertnisipeanu.androidnews.HomeScreen.HomeFragment;
import me.robertnisipeanu.androidnews.MainActivity;
import me.robertnisipeanu.androidnews.R;
import me.robertnisipeanu.androidnews.Tags;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoggedInFragment extends Fragment {

    FirebaseAuth mAuth;

    public LoggedInFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_account, container, false);

        ImageButton buttonLeft = view.findViewById(R.id.navButton_left);
        TextView navText = view.findViewById(R.id.navText);

        buttonLeft.setImageResource(R.drawable.ic_back);
        buttonLeft.setVisibility(View.VISIBLE);
        buttonLeft.setTag(Tags.BACK_BUTTON);
        buttonLeft.setOnClickListener(((MainActivity)getActivity())::onButtonBackClick);

        navText.setText(getString(R.string.account));

        mAuth = FirebaseAuth.getInstance();
        onVisibility(view);
        view.findViewById(R.id.button_logOut).setOnClickListener(this::onLogoutButtonClick);

        return view;
    }

    public void onVisibility(View view){
        ((TextView)view.findViewById(R.id.textView_account_email)).setText(mAuth.getCurrentUser().getEmail());
        ((TextView)view.findViewById(R.id.textView_account_name)).setText(mAuth.getCurrentUser().getDisplayName());
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(!hidden) onVisibility(getView());
    }

    public void onLogoutButtonClick(View v){
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        GoogleSignInClient client = GoogleSignIn.getClient(getActivity().getBaseContext(), gso);

        mAuth.signOut();
        client.signOut().addOnCompleteListener(task -> {
            ((HomeFragment)getActivity().getSupportFragmentManager().findFragmentByTag("HOME")).setModerator(false);
            ((MainActivity)getActivity()).goToPage("HOME");
        });

    }


}
