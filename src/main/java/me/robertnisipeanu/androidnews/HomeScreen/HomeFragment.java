package me.robertnisipeanu.androidnews.HomeScreen;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

import javax.annotation.Nullable;

import me.robertnisipeanu.androidnews.MainActivity;
import me.robertnisipeanu.androidnews.News;
import me.robertnisipeanu.androidnews.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private View view;
    private int lastSize=0;
    private ArrayList<News> news = new ArrayList<>();
    private NewsAdapter newsAdapter;

    private final static String TAG = "HomeFragment";

    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference newsCollection = db.collection("/news");
    private DocumentSnapshot lastDocument;
    private DocumentSnapshot firstDocument;

    private final int loadSize = 10;
    private boolean isLoading = false;

    public boolean isModerator = false;


    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.layout_home, container, false);

        initRecyclerView();
        initNews(loadSize);

        ImageButton buttonLeft = view.findViewById(R.id.navButton_left);
        TextView navText = view.findViewById(R.id.navText);

        buttonLeft.setImageResource(R.drawable.ic_loginicon);
        buttonLeft.setVisibility(View.VISIBLE);
        buttonLeft.setOnClickListener(this::onLoginButtonClick);
        navText.setText(getString(R.string.home));

        if(FirebaseAuth.getInstance().getCurrentUser() != null) moderatorCheck();

        return view;
    }

    private void listenForArticles(){
        newsCollection.whereEqualTo("visible", true).orderBy("dateCreated", Query.Direction.DESCENDING).endBefore(firstDocument).addSnapshotListener((queryDocumentSnapshots, e) -> {
            Log.d(TAG, "Snapshot Listener called");
            if(e != null){
                Log.w(TAG, "Error on snapshot listener", e);
                return;
            }

            for(DocumentChange documentChange : queryDocumentSnapshots.getDocumentChanges()){
                if(documentChange.getType() == DocumentChange.Type.ADDED){
                    News currentNews = documentChange.getDocument().toObject(News.class);
                    news.add(0, currentNews);
                    newsAdapter.notifyDataSetChanged();
                    Log.d(TAG, "New document: "+currentNews.getTitle());
                }
            }

        });
    }

    public void moderatorCheck(){
        db.collection("moderators").document(mAuth.getCurrentUser().getUid()).get().addOnCompleteListener(task -> {
            if(task.isSuccessful()){
                if(task.getResult().exists()) setModerator(true);
            }
        });
    }

    public void setModerator(boolean isModerator){
        this.isModerator = isModerator;
        newsAdapter.notifyDataSetChanged();
        if(isModerator) {
            Log.d(TAG, "User is a moderator");
            if (getView() == null) return;
            ImageButton buttonRight = getView().findViewById(R.id.navButton_right);
            buttonRight.setImageResource(R.drawable.ic_addarticle);
            buttonRight.setVisibility(View.VISIBLE);
            buttonRight.setOnClickListener(this::onAddStoryButtonClick);
        }
        else {
            if(getView() == null) return;
            ImageButton buttonRight = getView().findViewById(R.id.navButton_right);
            buttonRight.setVisibility(View.GONE);
        }
    }

    private void initNews(int size){


        Query query = newsCollection.whereEqualTo("visible", true).orderBy("dateCreated", Query.Direction.DESCENDING).limit(size);
        if(lastDocument != null) {
            query = query.startAfter(lastDocument);
            Log.d(TAG, "Starting after "+lastDocument.toObject(News.class).getTitle());
        }

        isLoading = true;
        query.get().addOnSuccessListener(queryDocumentSnapshots -> {
            if(queryDocumentSnapshots.size() == 0){
                Log.d(TAG, "No more documents to fetch");
                return;
            }
            for(DocumentSnapshot doc : queryDocumentSnapshots){
                News currentNews = doc.toObject(News.class);
                news.add(currentNews);
            }
            firstDocument = queryDocumentSnapshots.getDocuments().get(0);
            lastDocument = queryDocumentSnapshots.getDocuments().get(queryDocumentSnapshots.size() - 1);

            newsAdapter.notifyDataSetChanged();
            isLoading = false;
            listenForArticles();
        }).addOnFailureListener(e -> {
            Log.w(TAG, "Error fetching data from firebase: "+e.toString());
            isLoading = false;
        });

    }

    private void initRecyclerView() {
        RecyclerView recyclerView = view.findViewById(R.id.recyclerView_news);
        newsAdapter = new NewsAdapter(news, this);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setAdapter(newsAdapter);
        recyclerView.setLayoutManager(linearLayoutManager);

        RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();
                int visibleItemCount = linearLayoutManager.getChildCount();
                int totalItemCount = linearLayoutManager.getItemCount();

                if((firstVisibleItem + visibleItemCount == totalItemCount) && !isLoading) {
                    initNews(loadSize);
                    Log.d(TAG, "Loaded more items!");
                }

            }
        };
        recyclerView.addOnScrollListener(onScrollListener);

    }

    public void onLoginButtonClick(View view) {
        ((MainActivity) getActivity()).goToPage("LOGIN");
    }

    public void onAddStoryButtonClick(View view) {
        ((MainActivity) getActivity()).goToPage("ADD_STORY");
    }

    public void onDeleteClick(View view, int position){

        String docId = news.get(position).getId();
        news.remove(position);
        newsAdapter.notifyDataSetChanged();

        newsCollection.document(docId).update("visible", false).addOnSuccessListener(aVoid -> {
            Snackbar.make(getView(), "Article deleted successfully", Snackbar.LENGTH_SHORT).show();
            Log.d(TAG, "Removed item at position "+position);
        });

    }

    public void onArticleClick(View view, int position){
        Log.d(TAG, "Clicked article "+ news.get(position).getTitle());
        ((MainActivity)getActivity()).displayArticle(news.get(position));
    }

}
