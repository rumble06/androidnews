package me.robertnisipeanu.androidnews.HomeScreen;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.storage.FirebaseStorage;

import java.util.ArrayList;

import me.robertnisipeanu.androidnews.News;
import me.robertnisipeanu.androidnews.R;

public class NewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<News> news;
    private Context context;
    private Fragment fragment;

    public NewsAdapter(ArrayList<News> news, Fragment fragment) {
        this.news = news;
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;
        Log.d("NewsAdapter", "View type: "+i);

        switch(i){
            case 1:
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.news_item, viewGroup, false);
                return new NewsViewHolderImage(view);
            default:
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.news_item_noimage, viewGroup, false);
                return new NewsViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        News currentNews = news.get(i);
        /*newsViewHolder.textViewTitle.setText(currentNews.getTitle());
        newsViewHolder.textViewDescription.setText(currentNews.getDescription());
        if(newsViewHolder.imageView != null && currentNews.getImageUrl() != null) {
            Glide.with(fragment).load(currentNews.getImageUrl()).into(newsViewHolder.imageView);
        }*/
        String description = currentNews.getDescription().length() > 90 ? currentNews.getDescription().substring(0, 90) + "..." : currentNews.getDescription();
        switch (getItemViewType(i)){
            case 0:
                NewsViewHolder newsViewHolder = (NewsViewHolder) viewHolder;
                newsViewHolder.textViewTitle.setText(currentNews.getTitle());
                newsViewHolder.textViewDescription.setText(description);
                if(((HomeFragment)fragment).isModerator){
                    newsViewHolder.deleteButton.setVisibility(View.VISIBLE);
                    newsViewHolder.deleteButton.setOnClickListener(view -> ((HomeFragment)fragment).onDeleteClick(view, i));
                }
                else
                    newsViewHolder.deleteButton.setVisibility(View.GONE);
                newsViewHolder.cardView.setOnClickListener(view -> ((HomeFragment)fragment).onArticleClick(view, i));

                break;
            case 1:
                NewsViewHolderImage newsViewHolderImage = (NewsViewHolderImage) viewHolder;
                newsViewHolderImage.textViewTitle.setText(currentNews.getTitle());
                newsViewHolderImage.textViewDescription.setText(description);
                FirebaseStorage storage = FirebaseStorage.getInstance();
                if(currentNews.getImageUrl().startsWith("/")) storage.getReference(currentNews.getImageUrl()).getDownloadUrl().addOnSuccessListener(uri -> {
                    Log.d("NewsAdapter", uri.toString());
                    Glide.with(fragment).load(uri).into(newsViewHolderImage.imageView);
                });
                else Glide.with(fragment).load(currentNews.getImageUrl()).into(newsViewHolderImage.imageView);
                if(((HomeFragment)fragment).isModerator){
                    newsViewHolderImage.deleteButton.setVisibility(View.VISIBLE);
                    newsViewHolderImage.deleteButton.setOnClickListener(view-> ((HomeFragment)fragment).onDeleteClick(view, i));
                }
                else
                    newsViewHolderImage.deleteButton.setVisibility(View.GONE);
                newsViewHolderImage.cardView.setOnClickListener(view -> ((HomeFragment)fragment).onArticleClick(view, i));

                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(news.get(position).getImageUrl() == null) return 0;
        return 1;
    }

    @Override
    public int getItemCount() {
        return news.size();
    }

    class NewsViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;
        TextView textViewTitle;
        TextView textViewDescription;
        ImageButton deleteButton;

        public NewsViewHolder(@NonNull View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.layout_newsItem);
            textViewTitle = itemView.findViewById(R.id.textView_title);
            textViewDescription = itemView.findViewById(R.id.textView_description);
            deleteButton = itemView.findViewById(R.id.imageButton_deleteArticle);
        }
    }

    class NewsViewHolderImage extends RecyclerView.ViewHolder {

        CardView cardView;
        ImageView imageView;
        TextView textViewTitle;
        TextView textViewDescription;
        ImageButton deleteButton;

        public NewsViewHolderImage(@NonNull View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.layout_newsItem);
            imageView = itemView.findViewById(R.id.imageView_principal);
            textViewTitle = itemView.findViewById(R.id.textView_title);
            textViewDescription = itemView.findViewById(R.id.textView_description);
            deleteButton = itemView.findViewById(R.id.imageButton_deleteArticle);
        }

    }


}
