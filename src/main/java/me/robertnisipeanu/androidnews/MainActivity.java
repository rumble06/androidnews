package me.robertnisipeanu.androidnews;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;

import me.robertnisipeanu.androidnews.HomeScreen.HomeFragment;
import me.robertnisipeanu.androidnews.LoginScreen.LoggedInFragment;
import me.robertnisipeanu.androidnews.LoginScreen.LoginFragment;
import me.robertnisipeanu.androidnews.StoryScreen.AddStoryFragment;
import me.robertnisipeanu.androidnews.StoryScreen.StoryFragment;

public class MainActivity extends AppCompatActivity {

    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();

        goToPage("HOME");
    }

    public void goToPage(String page, String lastPage) {
        Fragment fragment;
        String fragmentTag;
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if(mAuth.getCurrentUser() == null) Log.d("MainActivity", "User is null");

        switch(page) {
            case "LOGIN":
                if(mAuth.getCurrentUser() == null) {
                    fragment = new LoginFragment();
                    fragmentTag = "LOGIN";
                }
                else {
                    fragment = new LoggedInFragment();
                    fragmentTag = "LOGGEDIN";
                }
                break;
            case "ADD_STORY":
                fragment = new AddStoryFragment();
                fragmentTag = "ADDSTORY";
                break;
            case "ARTICLE": // TEMPORARY
                fragment = new StoryFragment();
                fragmentTag = "STORY";
                break;
            default:
                fragment = new HomeFragment();
                fragmentTag = "HOME";
                break;
        }

        for(Fragment frag : getSupportFragmentManager().getFragments())
            transaction.hide(frag);

        if(getSupportFragmentManager().findFragmentByTag(fragmentTag) != null)
            transaction.show(getSupportFragmentManager().findFragmentByTag(fragmentTag));
        else {
            transaction.add(R.id.frameLayout_main, fragment, fragmentTag);
        }
        transaction.commit();
    }

    public void goToPage(String page) {
        goToPage(page, null);
    }

    public String getCurrentPage() {
        String currentPage = "";
        ArrayList<String> tags = new ArrayList<>();
        tags.add("LOGIN"); tags.add("LOGGEDIN"); tags.add("ADDSTORY"); tags.add("HOME");

        for(String fragmentTag : tags){
            Fragment verify = getSupportFragmentManager().findFragmentByTag(fragmentTag);
            if(verify != null && verify.isVisible()){
                return fragmentTag;
            }
        }

        return currentPage;
    }

    public void displayArticle(News article){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        for(Fragment frag : getSupportFragmentManager().getFragments())
            transaction.hide(frag);

        Fragment fragment = new StoryFragment();
        ((StoryFragment) fragment).setArticle(article);

        transaction.add(R.id.frameLayout_main, fragment, "STORY");
        transaction.commit();
    }

    public void onButtonBackClick(View v) {
        goToPage("HOME");
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if(getCurrentPage().equals("HOME")) super.onBackPressed();
        else goToPage("HOME");
    }
}
