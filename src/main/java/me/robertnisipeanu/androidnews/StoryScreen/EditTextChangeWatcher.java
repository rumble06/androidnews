package me.robertnisipeanu.androidnews.StoryScreen;

import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;

public class EditTextChangeWatcher implements TextWatcher {

    private TextInputLayout layout;

    public EditTextChangeWatcher(TextInputLayout layout){
        this.layout = layout;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if(layout.getError() != null){
            layout.setError(null);
            layout.setErrorEnabled(false);
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
