package me.robertnisipeanu.androidnews.StoryScreen;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.storage.FirebaseStorage;

import me.robertnisipeanu.androidnews.MainActivity;
import me.robertnisipeanu.androidnews.News;
import me.robertnisipeanu.androidnews.R;
import me.robertnisipeanu.androidnews.Tags;

/**
 * A simple {@link Fragment} subclass.
 */
public class StoryFragment extends Fragment {

    public static final String TAG = "StoryFragment";

    private News news;

    public StoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_article, container, false);

        ImageButton navButton_left = view.findViewById(R.id.navButton_left);
        ImageButton navButton_right = view.findViewById(R.id.navButton_right);
        TextView navText = view.findViewById(R.id.navText);
        navText.setVisibility(View.GONE);

        navButton_left.setImageResource(R.drawable.ic_back);
        navButton_left.setVisibility(View.VISIBLE);
        navButton_left.setTag(Tags.BACK_BUTTON);
        navButton_left.setOnClickListener(((MainActivity)getActivity())::onButtonBackClick);

        TextView articleTitle = view.findViewById(R.id.textView_article_title);
        TextView articleStory = view.findViewById(R.id.textView_article_story);
        ImageView imageView = view.findViewById(R.id.imageView_article);

        articleTitle.setText(news.getTitle());
        articleStory.setText(news.getDescription());

        if(news.getImageUrl() != null){
            if(news.getImageUrl().startsWith("/")) FirebaseStorage.getInstance().getReference(news.getImageUrl()).getDownloadUrl().addOnSuccessListener(uri -> {
                Glide.with(this).load(uri).into(imageView);
            });
            else Glide.with(this).load(news.getImageUrl()).into(imageView);
        }
        else imageView.setVisibility(View.GONE);

        return view;
    }

    public void setArticle(News news){
        this.news = news;
    }

}
