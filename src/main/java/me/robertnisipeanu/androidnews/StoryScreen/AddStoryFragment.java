package me.robertnisipeanu.androidnews.StoryScreen;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.IOException;
import java.sql.Time;
import java.util.Date;
import java.util.UUID;

import me.robertnisipeanu.androidnews.MainActivity;
import me.robertnisipeanu.androidnews.News;
import me.robertnisipeanu.androidnews.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddStoryFragment extends Fragment {

    private static final String TAG = "AddStoryFragment";
    private static final int READ_REQUEST_CODE = 42;

    Bitmap image = null;


    public AddStoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_addstory, container, false);

        ImageButton buttonLeft = view.findViewById(R.id.navButton_left);
        TextView navText = view.findViewById(R.id.navText);

        buttonLeft.setImageResource(R.drawable.ic_back);
        buttonLeft.setVisibility(View.VISIBLE);
        buttonLeft.setOnClickListener(((MainActivity)getActivity())::onButtonBackClick);
        navText.setText("Add story");

        view.findViewById(R.id.imageButton_pickImage).setOnClickListener(this::onPickImageButtonClick);
        view.findViewById(R.id.button_addStory).setOnClickListener(this::onAddStoryButtonClick);

        ((EditText)view.findViewById(R.id.editText_title)).addTextChangedListener(new EditTextChangeWatcher(view.findViewById(R.id.textInputLayout_title)));
        ((EditText)view.findViewById(R.id.editText_story)).addTextChangedListener(new EditTextChangeWatcher(view.findViewById(R.id.textInputLayout_story)));

        return view;
    }

    public void onAddStoryButtonClick(View view){
        view.setEnabled(false);
        TextInputLayout titleLayout = getView().findViewById(R.id.textInputLayout_title);
        EditText titleEditText = getView().findViewById(R.id.editText_title);
        TextInputLayout storyLayout = getView().findViewById(R.id.textInputLayout_story);
        EditText storyEditText = getView().findViewById(R.id.editText_story);

        boolean titleEmpty = false;
        boolean storyEmpty = false;

        if(titleEditText.getText().toString().trim().isEmpty()){
            titleLayout.setError("Title can't be empty!");
            titleLayout.setErrorEnabled(true);
            titleEmpty = true;
        }

        if(storyEditText.getText().toString().trim().isEmpty()){
            storyLayout.setError("Story can't be empty");
            storyLayout.setErrorEnabled(true);
            storyEmpty = true;
        }

        if(titleEmpty || storyEmpty) return;

        if(this.image != null){
            FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            this.image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] imageData = baos.toByteArray();
            UUID uuid = UUID.nameUUIDFromBytes(imageData);
            StorageReference storageReference = firebaseStorage.getReference(uuid.toString()+".jpg");
            storageReference.putBytes(imageData).addOnSuccessListener(taskSnapshot -> {
                String path = storageReference.getPath();
                Log.d(TAG, "Path: "+path);
                addArticle(titleEditText.getText().toString(), storyEditText.getText().toString(), path);
            });
        }
        else addArticle(titleEditText.getText().toString(), storyEditText.getText().toString());
    }

    public void addArticle(String title, String description){
        addArticle(title, description, null);
    }

    public void addArticle(String title, String description, String imagePath){
        FirebaseFirestore firestore = FirebaseFirestore.getInstance();
        Date timeNow = new Date();
        News news = new News(title, description, timeNow);
        news.setImageUrl(imagePath);
        news.setVisible(true);
        DocumentReference doc = firestore.collection("news").document();
        news.setId(doc.getId());
        doc.set(news).addOnSuccessListener(documentReference -> {
            ((TextView)getView().findViewById(R.id.editText_title)).setText("");
            ((TextView)getView().findViewById(R.id.editText_story)).setText("");
            getView().findViewById(R.id.button_addStory).setEnabled(true);
            Snackbar.make(getView(), "Article added successfully", Snackbar.LENGTH_SHORT).show();
            Log.d(TAG, "Successfully uploaded new article");
        });
    }

    public void onPickImageButtonClick(View view) {

        Log.d(TAG, "Pick image button was clicked!");

        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");

        startActivityForResult(intent, READ_REQUEST_CODE);

    }

    private void loadImage(Uri uri){
        try{
            this.image = getBitmapFromUri(uri);
            Log.d(TAG, "Loaded bitmap");
            ImageButton button = getView().findViewById(R.id.imageButton_pickImage);
            button.setImageBitmap(this.image);
        } catch(IOException e){
            Log.d(TAG, "Error loading image!");
            Snackbar.make(getView(), "Error loading image, please try again!", Snackbar.LENGTH_SHORT);
        }
    }

    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                getActivity().getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == READ_REQUEST_CODE){
            if(resultCode == Activity.RESULT_OK) {
                Uri uri = data.getData();
                Log.d(TAG, "Uri to image: "+uri.toString());
                loadImage(uri);
            }
        }

    }
}
